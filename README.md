Basic Shell Commands
==================


Init Submodules
-------------

	git submodule update --init


Create VM / Start Vagrant
----------------------

	vagrant up


Destroy VM
---------

	vagrant destroy

Confirm with ´y´


SSH connect to vagrant VM
----------------------

	vagrant ssh
