name "magento"
version "0.1.0"
description "A Chef cookbook for deployment of Magento with Vagrant."

supports "debian"

depends "apt"
depends "build-essential"
depends "git"
depends "mysql"
depends "database"
depends "php"
depends "apache2"

recipe "magento", "Main configuration for Magento"