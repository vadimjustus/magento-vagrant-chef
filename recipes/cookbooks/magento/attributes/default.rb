# Default attributes.
default['magento']['mage']['install_dir'] = "/var/www/"            # Where to install magento
default['magento']['mage']['project_name'] = "magento"            # Where to install magento
default['magento']['mage']['dir'] = "#{default['magento']['mage']['install_dir']}/#{default['magento']['mage']['project_name']}"             # magento base dir

default['magento']['phpinfo_enabled'] = false                      #add an alias for a /phpinfo.php file
default['magento']['mage_check_enabled'] = false                   #add an alias for a /magento-check.php file
default['magento']['mage_dev_enabled'] = false                     # Enables Magento Developer mode (errors on)

default['magento']['source']['install'] = true
default['magento']['source']['url'] = "http://10.0.2.57/magento/tmp"  # Default Magento versions repo used
default['magento']['source']['type'] = "tar.gz"                   # Extension used by Magento versions. Possible values are tar.bz2, tar.gz
default['magento']['source']['version'] = "magento-enterprise-1.13.1.0"

default['magento']['sample_data']['install'] = false               #install Magento sample data
default['magento']['sample_data']['url'] = "http://10.0.2.57/magento/tmp"  # Default url used to download sample data
default['magento']['sample_data']['type'] = "tar.gz"                   # Extension used by Magento versions. Possible values are tar.bz2, tar.gz, tgz
default['magento']['sample_data']['version'] = 'enterprise'           # Default Magento sample data version

# TODO: Add support for reindex, clear cache
default['magento']['reindex'] = false                              #reindex Magento once deployed
default['magento']['clearcache'] = false                           #clear Magento cache once deployed

default['magento']['config']['install'] = false                    #install magento database via magento install
default['magento']['config']['locale'] = "de_DE"                   #required, Time Zone
default['magento']['config']['timezone'] = "Europe/Berlin"   #required, Time Zone
default['magento']['config']['default_currency'] = "EUR"           #required, Default Currency

default['magento']['config']['db_host'] = "localhost"              #required, You can specify server port, ex.: localhost:3306
                                                                           #If you are not using default UNIX socket, you can specify it
                                                                           #here instead of host, ex.: /var/run/mysqld/mysqld.sock
                                                                           
default['magento']['config']['db_model'] = "mysql4"                #Database type (mysql4 by default)
default['magento']['config']['db_name'] = "magento"                #required, Database Name
default['magento']['config']['db_user'] = "root"                   #required, Database User Name
default['magento']['config']['db_pass'] = "root"                   #required, Database User Password
default['magento']['config']['db_prefix'] = ""                     #optional, Database Tables Prefix
                                                                           #No table prefix will be used if not specified
                                                                           
default['magento']['config']['session_save'] = "files"             #optional, where to store session data - in db or files. files by default
default['magento']['config']['admin_frontname'] = ""               #optional, admin panel path, "admin" by default
default['magento']['config']['url'] = "magento.mv"                 #required, URL the store is supposed to be available at
default['magento']['config']['skip_url_validation'] = "yes"        #optional, skip validating base url during installation or not. No by default
default['magento']['config']['use_rewrites'] = "yes"               #optional, Use Web Server (Apache) Rewrites,
                                                                           #You could enable this option to use web server rewrites functionality for improved SEO
                                                                           #Please make sure that mod_rewrite is enabled in Apache configuration
                                                                           
default['magento']['config']['use_secure'] = "no"                  #optional, Use Secure URLs (SSL). Enable this option only if you have SSL available.
default['magento']['config']['secure_base_url'] = "{{base_url}}"   #optional, Secure Base URL
                                                                           #Provide a complete base URL for SSL connection.
                                                                           #For example: https://www.mydomain.com/magento/
                                                                           
default['magento']['config']['use_secure_admin'] = "no"            #optional, Run admin interface with SSL
default['magento']['config']['enable_charts'] = ""                 #optional, Enables Charts on the backend's dashboard

default['magento']['config']['encryption_key'] = ""                #optional, will be automatically generated and displayed on success, if not specified

default['magento']['config']['admin_username'] = "admin"      #required, admin user login
default['magento']['config']['admin_password'] = "eraZor00"         #required, admin user password
default['magento']['config']['admin_email'] = "test@example.com"   #required, admin user email
default['magento']['config']['admin_firstname'] = "Admin"          #required, admin user first name
default['magento']['config']['admin_lastname'] = "User"            #required, admin user last name

# Magento extensions
default['magento']['modman']['url'] = "https://raw.github.com/colinmollenhour/modman/master/modman"
default['magento']['debug']['enabled'] = false
default['magento']['debug']['repository'] = "https://github.com/madalinoprea/magneto-debug.git"

# Awesome tool for Magento
default['magento']['n98-magerun']['enabled'] = true
default['magento']['n98-magerun']['repository'] = "https://raw.github.com/netz98/n98-magerun/master/n98-magerun.phar"


#override attributes for our included recipes
override['build_essential']['compiletime'] = true
override['mysql']['server_root_password'] = node['magento']['config']['db_pass']
override['mysql']['allow_remote_root'] = true
override['mysql']['tunable']['key_buffer'] = "64M"
override['mysql']['tunable']['innodb_buffer_pool_size'] = "32M"

override['mysql']['server_root_password'] = node['magento']['config']['db_pass']

node['mysql']['server_repl_password'] = "root"
node['mysql']['server_debian_password'] = "root"