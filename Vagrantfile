# -*- mode: ruby -*-
# vi: set ft=ruby :

PROJECT_NAME = "warema.com"
SUBCONTEXT = PROJECT_NAME.gsub(".", "").capitalize + "vm"

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.require_version ">= 1.5.4"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.box = "chef/debian-7.4"
  #config.vm.box = "precise64"
  #config.vm.box_url = 'http://files.vagrantup.com/precise64.box'

  config.vm.hostname = PROJECT_NAME + ".vm"

  # Create a private network, which allows host-only access to the machine
  #config.vm.network "private_network", ip: "10.20.30.40"
  config.vm.network :private_network, ip: "10.20.30.40"

  # disable default shared folder
  config.vm.synced_folder ".", "/vagrant", disabled: true

  # use your local SSH keys / agent even within the virtual machine
  config.ssh.forward_agent = true

  # sync Chef certificates into VM
  #config.vm.synced_folder "~/.chef/" + PROJECT_NAME, "/etc/chef/certificates", type: "rsync", rsync__auto: false

  # share site folder into releases folder
  config.vm.synced_folder ".", "/var/www/" + config.vm.hostname + "/releases/vagrant",
    type: "rsync",
    group: "web",
    rsync__args: ["--verbose", "--archive", "--delete", "-z", "--chmod=Dg+s,Dg+x"],
    rsync__exclude: [
      ".git/",
      ".idea/",
      ".DS_Store",
      "Vagrantfile"
    ]

  # adjust VirtualBox specific settings
  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "2048"]
  end

  # Make sure the chef server can be contacted (private IP does not work)
  #config.vm.provision "shell", inline: "grep chef.tdintern.de /etc/hosts || (echo '" + Socket::getaddrinfo("tdkol.ignorelist.com", "https", nil, Socket::SOCK_STREAM)[0][3] + " chef.tdintern.de' >> /etc/hosts)"

  # Install chef client
  config.vm.provision "shell", inline: "dpkg -s chef > /dev/null 2>&1 || (wget -O - http://opscode.com/chef/install.sh | sudo /bin/sh)"

  # quick hack for ES setup
  # TODO remove after proper Chef setup
  config.vm.provision "shell", inline: "aptitude install -y openjdk-6-jre-headless"
  #config.vm.provision "shell", inline: "dpkg -s elasticsearch > /dev/null 2>&1 || (cd /tmp && wget -nv -O es.deb https://download.elasticsearch.org/elasticsearch/elasticsearch/elasticsearch-1.1.0.deb && dpkg -i es.deb && rm es.deb)"
  #config.vm.provision "shell", inline: "update-rc.d elasticsearch defaults 95 10"
  #config.vm.provision "shell", inline: "/etc/init.d/elasticsearch start"

  # Enable provisioning with chef server
  #config.vm.provision "chef_client" do |chef|
  #  chef.chef_server_url = "#{ENV['KNIFE_CHEF_SERVER']}"
  #  chef.validation_key_path = "#{ENV['KNIFE_VALIDATION_CLIENT_KEY']}"
  #  chef.client_key_path = "/etc/chef/certificates/" + config.vm.hostname + ".pem"
  #  chef.node_name = config.vm.hostname
  #end

  config.vm.provision :chef_solo do |chef|
    # Specify vagrant cookbooks: our project cookbooks and magento required cookbooks
    chef.cookbooks_path = ["./recipes/cookbooks",]
    #chef.roles_path = "./recipes/roles"
    #chef.data_bags_path = "./recipes/data_bags"
    chef.add_recipe "magento"

    # You may also specify custom JSON attributes:
    chef.json = {
        'magento' => {
            'mage' => {
                'install_dir' => '/var/www',
                'project_name' => config.vm.hostname
            },
            'config' => {
                'install' => true,
            },
            'source' => {
                'version' => 'magento-enterprise-1.13.1.0',
            },
            'sample_data' => {
                'install' => true,
                'version' => 'enterprise',
            },
            # Sets MAGE_IS_DEVELOPER_MODE (error reporting)
            'mage_dev_enabled' => true,

            # Install Magneto Debug
            'debug' => {
                'enabled' => true,
            }
        },
        'php' => {
            'version' => '5.4.27'
        }
    }
    chef.log_level = :debug
  end
end
